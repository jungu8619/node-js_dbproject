var express = require('express');
var router = express.Router();

/*로그인 인증 관련 시작 */
var passport = require('../support/login').passport;

/*로그인 인증 관련 끝 */

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('home/main');
});
router.get('/ipfind', function (req, res, next) {
    res.render('home/IPfind');
});

router.get('/login', function (req, res, next) {
    res.render('home/login');
});

//로그인
router.post('/login',passport.authenticate('local', {
    successRedirect: '/main', // 로그인 성공 Redirect URL
    failureRedirect: '/login', // 로그인 실패 Redirect URL
}));

module.exports = router;
