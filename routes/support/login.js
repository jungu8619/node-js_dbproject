/**
 * Created by kangjungu1 on 2016. 10. 14..
 */

//http://www.messier219.com/2016/02/25/passport-js/

'use strict';
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var connection = require('./connection').pool;
var loginQuery = require('./connection').loginQuery;

passport.use(new LocalStrategy({
    usernameField: 'user_id',
    passwordField: 'user_password'
}, function (login_id, password, done) {
    connection.getConnection(function (err, connection) {
        console.log("connection.getConnection");
        connection.query(loginQuery, [login_id], function (err, rows) {
            var user = rows[0];
            if (err) {
                //에러
                return done(err);
            }
            if (!user) {
                //유저 없는경우
                console.error("유저 없음 ");
                // res.send('<script>alert("로그인 실패")</script>')
                return done(null, false, {message: "Incorrect username."});
            }
            if (user.password != password) {
                //패스워드가 맞지 않은경우
                console.error("패스워드 맞지 않음");
                return done(null, false, {message: "Incorrect password."});
            }

            var u = {id: user.login_id};

            return done(null, u);
        });
    })
}));

// 인증 후, 사용자 정보를 Session에 저장함
passport.serializeUser(function(user, done) {
    console.error("serializeUser")
    done(null, user);
});

// 인증 후, 페이지 접근시 마다 사용자 정보를 Session에서 읽어옴.
passport.deserializeUser(function(user, done) {
    console.error("deserializeUser")
    done(null, user);
});


module.exports.passport = passport;
//TODO: 이렇게 사용할수 있는지 물어보기
module.exports.loginAuth = function(req,res,next){
    var isLogin = req.isAuthenticated();
    if(!isLogin) {
        res.redirect('/login');
    }else{
        //next()를 해줘야지 다음 url들을 확인한다
        next();
    }
}
