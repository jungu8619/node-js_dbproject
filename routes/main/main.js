var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/',function(req,res,next){
    //로그인됐는지 확인
    var isLogin = req.isAuthenticated();
    console.error("is login "+isLogin)

    if(isLogin) {
        res.render('main/main');
    }else{
        res.redirect('/login');
    }
});


module.exports = router;
