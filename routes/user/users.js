var express = require('express');
var router = express.Router();
/* GET users listing. */

//router.use를 사용해서 로그인 했는지 안했는지 한번 코딩으로 확인 가능
router.use('/',function(req,res,next){
  //login.js에 선언되어있는 loginAuth 함수 실행
  require('../support/login').loginAuth(req,res,next);
});

router.get('/contacts', function(req, res, next) {
  res.render('user/contacts');
});
router.get('/profile',function(req,res,next){
  res.render('user/profile');
});
router.get('/edit',function(req,res,next){
  res.render('user/edit');
});

module.exports = router;
